
## Badges

Add badges from somewhere like: [shields.io](https://shields.io/)

[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://github.com/tterb/atomic-design-ui/blob/master/LICENSEs)
[![GPLv3 License](https://img.shields.io/badge/License-GPL%20v3-yellow.svg)](https://opensource.org/licenses/)
[![AGPL License](https://img.shields.io/badge/license-AGPL-blue.svg)](http://www.gnu.org/licenses/agpl-3.0)

  
# Project Title

A brief description of what this project does and who it's for


## Authors

- [@katherinepeterson](https://www.github.com/octokatherine)

  
## Tech Stack

**Client:** React, Redux, TailwindCSS

**Server:** Node, Express

  
## Contributing

Contributions are always welcome!

See `contributing.md` for ways to get started.

Please adhere to this project's `code of conduct`.

  
## Deployment

To deploy this project run

```bash
  npm run deploy
```

  
## Features

- Light/dark mode toggle
- Live previews
- Fullscreen mode
- Cross platform

  
## Documentation

[Documentation](https://linktodocumentation)

  
## Screenshots

![App Screenshot](https://via.placeholder.com/468x300?text=App+Screenshot+Here)

  
## Usage/Examples

```javascript
import Component from 'my-project'

function App() {
  return <Component />
}
```

  
## Feedback

If you have any feedback, please reach out to us at fake@fake.com

  